import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  api = "http://localhost:8080/api";

  constructor(
    private http: HttpClient
  ) { }

  obtener(){
    return this.http.get<empleado[]>(this.api+"/empleados");
  }

  agregar(empleado: empleado){
    return this.http.post(this.api+"/empleado",empleado);
  }

  eliminar(id: number){
    return this.http.delete(this.api+"/eliminar/"+id);
  }


  
  editar(empleado: empleado){
    
    return this.http.put(this.api+"/editarEmpleado",empleado);
  }
}

export interface empleado{

  id?: number
  nombre: string,
  brm: string,
  puesto: string,
  foto: string

}
